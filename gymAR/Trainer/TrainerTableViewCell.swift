//
//  TrainerTableViewCell.swift
//  gymAR
//
//  Created by NICOLAS EDUARDO CABRERA VENEGAS on 10/30/18.
//  Copyright © 2018 user145541. All rights reserved.
//

import UIKit

class TrainerTableViewCell: UITableViewCell {

    @IBOutlet weak var clientImage: UIImageView!
    @IBOutlet weak var clientNameLabel: UILabel!
    
    @IBOutlet weak var clientLocationLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        clientImage.layer.cornerRadius = clientImage.frame.size.width/2
        clientImage.clipsToBounds = true
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
