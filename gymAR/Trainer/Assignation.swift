//
//  Assignation.swift
//  gymAR
//
//  Created by NICOLAS EDUARDO CABRERA VENEGAS on 10/30/18.
//  Copyright © 2018 user145541. All rights reserved.
//

import UIKit

class Assignation{
    
    var user: String
    var exercise: String
    var img: UIImage
    var fecha: String
    var docId: String
    var condiciones: String
    
    init(user: String, exercise: String, img: UIImage, fecha: String, docId: String, condiciones: String) {
        self.user = user
        self.exercise = exercise
        self.img = img
        self.fecha = fecha
        self.docId = docId
        self.condiciones = condiciones
    }
    
}
