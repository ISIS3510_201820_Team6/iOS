//
//  Category.swift
//  gymAR
//
//  Created by Juan Diego González on 11/5/18.
//  Copyright © 2018 user145541. All rights reserved.
//

import UIKit
import os.log

class Category: NSObject, NSCoding{
    
    var name : String
    var icon : UIImage?
    
    
    
    //MARK: Archiving Paths
    static let DocumentsDirectory = FileManager().urls(for: .documentDirectory, in: .userDomainMask).first!
    static let ArchiveURL = DocumentsDirectory.appendingPathComponent("categories")
    
    //MARK: Types
    struct PropertyKey{
        static let name = "name"
        static let icon = "icon"
    }
    
    //MARK: Initialization
    init?(name: String, icon: UIImage){
        guard !name.isEmpty else{
            return nil
        }
        
        //Inicialización
        self.name = name
        self.icon = icon
    }
    
    func encode(with aCoder: NSCoder){
        aCoder.encode(name, forKey: PropertyKey.name)
        aCoder.encode(icon, forKey: PropertyKey.icon)
    }
    
    required convenience init?(coder aDecoder: NSCoder){
        
        guard let name = aDecoder.decodeObject(forKey: PropertyKey.name) as? String else {
            os_log("Unable to decode the name for a Exercise object.", log: OSLog.default, type: .debug)
            return nil
        }
        
        let icon = aDecoder.decodeObject(forKey: PropertyKey.icon) as! UIImage
        
        self.init(name: name, icon: icon)
        
    }
    
}
