//
//  Trainer.swift
//  gymAR
//
//  Created by NICOLAS EDUARDO CABRERA VENEGAS on 10/30/18.
//  Copyright © 2018 user145541. All rights reserved.
//

import Foundation

class Trainer{
    
    var name: String
    var zone: String
    
    init(name: String, zone: String) {
        self.name = name
        self.zone = zone
    }
}
