//
//  BarChartViewController.swift
//  gymAR
//
//  Created by Juan Diego González on 11/3/18.
//  Copyright © 2018 user145541. All rights reserved.
//

import UIKit
import Charts
import Firebase
import os.log


class BarChartViewController: UIViewController, UITableViewDelegate,UITableViewDataSource  {
    let listName = ["Abs Rollout", "Planking", "Caminadora"]
    let listImg = [UIImage(named:"rolloutIcon"), UIImage(named:"rolloutIcon"), UIImage(named:"rolloutIcon")]
    
    @IBOutlet weak var tableView: UITableView!
    
    var months: [String]!
    var muscles = [String]()
    var values = [[Double]]()
    var listaRecomendados = [Exercise]()
    var losDatazos : Query!
    var queryRecomendados : Query!
    var losDatos : MuscleValue!
    
    
    
    @IBOutlet weak var barChartView: BarChartView!
    override func viewDidLoad() {
        super.viewDidLoad()
//        self.navigationController!.navigationBar.topItem!.title = "Atras"
        let myId = UserDefaults.standard.value(forKey: "userID") as? String ?? ""
        let elDocRef = Firestore.firestore().document("usuarios/\(myId)")
        losDatazos = Firestore.firestore().collection("bodyStatistics").whereField("user", isEqualTo:  elDocRef)
        queryRecomendados = Firestore.firestore().collection("ejercicios").limit(to: 3)
        print("\(myId)")
        
 

        DispatchQueue.global(qos: .background).async{
            self.traerDatos()
            DispatchQueue.main.async {
                print("este es muscles \(self.muscles.count)")
                print("este es values \(self.values.count)")

            }
        }
        
        DispatchQueue.global(qos: .background).async{
            self.loadRecomendados()
//            DispatchQueue.main.async{
//            }
        
         }

        
        
        // Do any additional setup after loading the view
    }
    
    func setChart(dataPoints: [String], values: [[Double]]){
        var dataEntries: [BarChartDataEntry] = []
        let formatter = BarChartFormatter()
        formatter.setValues(values:dataPoints)
        let xaxis:XAxis = XAxis()
        barChartView.noDataText = "Cargando datos..."
        
        print(dataPoints.count)
        print(values.count)
        for i in 0..<dataPoints.count{
            
            let dataEntry = BarChartDataEntry(x: Double(i), yValues: values[i], data: dataPoints[i] as AnyObject)
            dataEntries.append(dataEntry)
        }
        
        let chartDataSet = BarChartDataSet(values: dataEntries, label: nil)
        chartDataSet.colors = [UIColor(red: 151/255, green: 145/255, blue: 144/255, alpha: 1.0), UIColor(red: 244/255 , green: 76/255, blue: 54/255  , alpha: 1.0), UIColor(red: 76/255, green: 218/255, blue: 79/255, alpha: 1.0)]
        chartDataSet.valueTextColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.0)
        
        let chartData = BarChartData(dataSet: chartDataSet)
        
        
        xaxis.valueFormatter = formatter
        barChartView.xAxis.labelPosition = .bottom
        barChartView.xAxis.drawGridLinesEnabled = false
        barChartView.xAxis.valueFormatter = xaxis.valueFormatter
        barChartView.xAxis.granularityEnabled = true
        barChartView.xAxis.granularity = 1.0
        barChartView.xAxis.decimals = 0
        barChartView.chartDescription?.enabled = false
        barChartView.legend.enabled = false
        barChartView.rightAxis.enabled = false
        barChartView.data = chartData
        
        barChartView.animate(xAxisDuration: 2.0, yAxisDuration: 2.0)
        
    }
    
    func traerDatos(){
        
        if CheckNetwork.Connection(){
            losDatazos.addSnapshotListener{ (querySnapshot, error) in
                guard let snapshot = querySnapshot else {
                    print("Error fetching snapshots: \(error!)")
                    return
                }
                self.muscles = ["Torax", "Pantorrilla", "Brazo", "Antebrazo"]
                for document in querySnapshot!.documents{
                    let data = document.data()
                    let torax = data["torax"] as! [Double]
                    let pantorrilla = data["pantorrilla"] as! [Double]
                    let brazo = data["brazo"] as! [Double]
                    let anteBrazo = data["antebrazo"] as! [Double]
                    
                    self.values.append(torax)
                    self.values.append(pantorrilla)
                    self.values.append(brazo)
                    self.values.append(anteBrazo)
                }
                self.setChart(dataPoints: self.muscles, values: self.values)
                self.losDatos = MuscleValue(muscles: self.muscles, values: self.values)
                print("\(self.losDatos.muscles.count) estoy acá")
                if self.losDatos.muscles.count > 0 {
                    print("\(self.losDatos.muscles.count) asdffff")
                    self.saveData()
                }
                
            }
            
            
        }else{
            createAlertNetwork(title: "No estás conectado a una red de Internet", message: "Los datos que aparecen en pantalla pueden que no sean los últimos")
            if let losQueSon = self.loadData(){
                print("\(losQueSon) jejejeje")
                if values.count == 0 {
                    self.values = losQueSon.values
                    self.setChart(dataPoints: losQueSon.muscles, values: self.values)
                }
            }
        }
        
    }
    func loadRecomendados(){
        if CheckNetwork.Connection() {
            print("Cbro")
            queryRecomendados.addSnapshotListener{ (querySnapshot, err) in
                if let err = err{
                    print("Error getting documents: \(err)")
                }else{
                    for document in querySnapshot!.documents{
                        let data = document.data()
                        let nombre = data["nombre"] as? String ?? ""
                        let nUrlVideo = data["video"] as? String ?? ""
                        let descripcion = data["descripcion"] as? String ?? ""
                        let imgUrl = data["foto"] as? String ?? ""
                        let iconUrl = data["iconoIOS"] as? String ?? ""
                        let zone = data["zona"] as? String ?? ""
                        DispatchQueue.global().async {
                            do {
                                let urlImage = URL(string: imgUrl)
                                let urlIcon = URL(string: iconUrl)
                                let dataImage = try Data(contentsOf: urlImage!)
                                let dataIcon = try Data(contentsOf: urlIcon!)
                                let img = UIImage(data: dataImage)!
                                let icon = UIImage(data: dataIcon)!
                                let exerciseObj = Exercise(name: nombre, urlVideo: nUrlVideo, description: descripcion, photo: img, icon: icon, zone: zone)
                                self.listaRecomendados.append(exerciseObj!)
                                DispatchQueue.main.sync {
                                    self.tableView.reloadData()
                                }
                            }
                            catch{
                                print(error)
                            }
                            
                        }
                        
                        //                        self.exercises.append(exerciseObj!)


                    }
                    if self.listaRecomendados.count > 0 {
                        self.saveExercise()
                        self.tableView.reloadData()

                    }
                }
            }
        } else {
            createAlertNetwork(title: "Your phone has no Internet connection", message: "In order to help you out in your workout we will display some exercises that where previously loaded.")
            
            if let losQueSon = self.loadExercises(){
                print("hola")
                print("\(losQueSon.count)")
                if listaRecomendados.count == 0{
                    for exe in losQueSon{
                        let nombre = exe.name
                        let nUrlVideo = exe.urlVideo ?? ""
                        let descripcion = exe.descrip ?? ""
                        let img = exe.photo
                        let icon = exe.icon
                        let zone = exe.zone as? String ?? ""
                        let exeObject = Exercise(name: nombre, urlVideo: nUrlVideo, description: descripcion, photo: img!, icon: icon!, zone: zone)
                        self.listaRecomendados.append(exeObject!)

                    }
                }
            }
        }
        
    }
    
    
    private func saveExercise(){
        let isSuccessfulSave = NSKeyedArchiver.archiveRootObject(listaRecomendados, toFile: Exercise.ArchiveURL.path)
        if isSuccessfulSave{
            os_log("Exercises successfully saved", log: OSLog.default, type: .debug)
        } else {
            os_log("Failed to save exercises", log: OSLog.default, type: .error)
        }
    }
    
    private func loadExercises()-> [Exercise]?{
        return NSKeyedUnarchiver.unarchiveObject(withFile: Exercise.ArchiveURL.path) as? [Exercise]
    }
    
    
    private func saveData(){
        let isSuccessfulSave = NSKeyedArchiver.archiveRootObject(losDatos, toFile: MuscleValue.ArchiveURL.path)
        if isSuccessfulSave{
            os_log("Exercises successfully saved", log: OSLog.default, type: .debug)
        } else {
            os_log("Failed to save exercises", log: OSLog.default, type: .error)
        }
        
    }
    
    private func loadData()-> MuscleValue?{
        return NSKeyedUnarchiver.unarchiveObject(withFile: MuscleValue.ArchiveURL.path) as? MuscleValue
    }
    func createAlertNetwork(title: String, message: String ){
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default , handler: { (action) in
            alert.dismiss(animated: true, completion: nil)
        }))
        
        self.present(alert, animated: true, completion: nil)
        
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return(listaRecomendados.count)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellIdentifier = "recoCell"
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? ExerciseTableViewCell  else {
            fatalError("The dequeued cell is not an instance of ExerciseTableViewCell.")
        }
        
        cell.nameLabel.text = listaRecomendados[indexPath.row].name
        cell.imageIcon.image = listaRecomendados[indexPath.row].icon
        
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        let photo2 = UIImage(named: "myBodyImg")
        if segue.identifier == "segueDetail2"{
            if let indexPath = tableView.indexPathForSelectedRow{
                let destVC = segue.destination as! ExerciseViewController
                destVC.name = listaRecomendados[indexPath.row].name
                destVC.desc = listaRecomendados[indexPath.row].descrip
                destVC.image = listaRecomendados[indexPath.row].photo
                destVC.videoUrl = listaRecomendados[indexPath.row].urlVideo
                destVC.zona = listaRecomendados[indexPath.row].zone
                print("\(listaRecomendados[indexPath.row].description)")
            }
        }
    }
    
    @objc(BarChartFormatter)
    public class BarChartFormatter: NSObject, IAxisValueFormatter {
        var names = [String]()
        
        public func stringForValue(_ value: Double, axis: AxisBase?) -> String {
            return names[Int(value)]
        }
        
        public func setValues(values: [String]) {
            self.names = values
        }
    }
    
    
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
