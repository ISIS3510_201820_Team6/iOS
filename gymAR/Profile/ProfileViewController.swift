//
//  ProfileViewController.swift
//  gymAR
//
//  Created by user145541 on 10/1/18.
//  Copyright © 2018 user145541. All rights reserved.
//

import UIKit
import Firebase
import os.log

class ProfileViewController: UIViewController {

    @IBOutlet weak var profileImg: UIImageView!
    @IBOutlet weak var weightImg: UIImageView!
    @IBOutlet weak var fatImg: UIImageView!
    @IBOutlet weak var MaxHRImg: UIImageView!
    @IBOutlet weak var IMCImg: UIImageView!
    @IBOutlet weak var myBody: UIButton!
    @IBOutlet weak var myStats: UIButton!
    @IBOutlet weak var nameLabel: UILabel!
    var weightLabel: UILabel!
    var fatLabel: UILabel!
    var maxHRLabel: UILabel!
    var IMCLabel: UILabel!
    var user: User!
    var docRef: DocumentReference!
    var us: User!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let uid = UserDefaults.standard.value(forKey: "userID")
        docRef = Firestore.firestore().document("usuarios/\(uid!)")
        //Hacer imagenes circulares o con bordes redondeados
        profileImg.layer.cornerRadius = profileImg.frame.size.width/2
        profileImg.clipsToBounds = true
        weightImg.layer.cornerRadius = weightImg.frame.size.width/2
        weightImg.clipsToBounds=true
        fatImg.layer.cornerRadius = fatImg.frame.size.width/2
        fatImg.clipsToBounds=true
        MaxHRImg.layer.cornerRadius = weightImg.frame.size.width/2
        MaxHRImg.clipsToBounds=true
        IMCImg.layer.cornerRadius = weightImg.frame.size.width/2
        IMCImg.clipsToBounds=true
//        myBodyImg.layer.cornerRadius = myBodyImg.frame.size.width/5
//        myBodyImg.clipsToBounds=true
//        specialConditionsImg.layer.cornerRadius = myBodyImg.frame.size.width/5
//        specialConditionsImg.clipsToBounds=true


        //Agregar labels a sus respectivas imagenes
        weightLabel = UILabel(frame: CGRect(x: 0, y: 0, width: 75, height: 75))
        weightLabel.textAlignment = .center
        self.weightImg.addSubview(weightLabel)
        self.weightImg.bringSubview(toFront: weightLabel)
        
        fatLabel = UILabel(frame: CGRect(x: 0, y: 0, width: 75, height: 75))
        fatLabel.textAlignment = .center
        fatLabel.textColor = .black
        self.fatImg.addSubview(fatLabel)
        self.fatImg.bringSubview(toFront: fatLabel)
        
        maxHRLabel = UILabel(frame: CGRect(x: 0, y: 0, width: 75, height: 75))
        maxHRLabel.textAlignment = .center
        maxHRLabel.textColor = .black
        self.MaxHRImg.addSubview(maxHRLabel)
        self.MaxHRImg.bringSubview(toFront: maxHRLabel)
        
        IMCLabel = UILabel(frame: CGRect(x: 0, y: 0, width: 75, height: 75))
        IMCLabel.textAlignment = .center
        IMCLabel.textColor = .black
        self.IMCImg.addSubview(IMCLabel)
        self.IMCImg.bringSubview(toFront: IMCLabel)
        //Cargar datos de firebase(temporal)

        let usuarioCargado = user
        if usuarioCargado == nil {
            if CheckNetwork.Connection(){
                loadUserData()
            }else{
                createAlertNetwork(title: "Your phone is has no network connection", message: "In order to help you out in your workout we will display some exercises that where previously loaded.")
            }
        }else{
            user = usuarioCargado
            showUserInfo()
            //updateUserData()
        }
        let myBodyLabel = UILabel(frame: CGRect(x: 0, y: 0, width: 150, height: 150))
        myBodyLabel.textAlignment = .center
        myBodyLabel.text = "My Body"
        myBodyLabel.textColor = .white
//        self.myBodyImg.addSubview(myBodyLabel)
//        self.myBodyImg.bringSubview(toFront: myBodyLabel)
        
        let myConditionLabel = UILabel(frame: CGRect(x: 0, y: 0, width: 150, height: 150))
        myConditionLabel.textAlignment = .center
        myConditionLabel.numberOfLines = 2
        myConditionLabel.text = "My Physical \n Condition"
        myConditionLabel.textColor = .white
//        self.specialConditionsImg.addSubview(myConditionLabel)
//        self.specialConditionsImg.bringSubview(toFront: myConditionLabel)
    }
    
    func loadUserData(){
        
        docRef.addSnapshotListener{(docSnapshot, error) in
            guard let docSnapshot = docSnapshot, docSnapshot.exists else {return}
            let data = docSnapshot.data()
            let name = data?["nombre"] as? String ?? ""
            let IMC = data?["IMC"] as? Double ?? 0.0
            let fat = data?["grasaCorporal"] as? Double ?? 0.0
            let weight = data?["peso"] as? Double ?? 0.0
            let maxHR = data?["maxHR"] as? Int ?? 0
            let lastName = data?["apellido"] as? String ?? ""
            let imagenUrl = data?["imagen"] as? String ?? ""
            do {
                let urlImage = URL(string: imagenUrl)
                let dataImage = try Data(contentsOf: urlImage!)
                let img = UIImage(data: dataImage)!
                self.user = User(name: name, lastName: lastName, weight: weight, fat: fat, IMC: IMC, maxHR: maxHR, photo: img)
                self.saveUser()
                self.showUserInfo()
            }catch{
                print(error)
            }
            

        }
    }
    
    @IBAction func toMyBody(_ sender: Any) {
        if CheckNetwork.Connection(){
            performSegue(withIdentifier: "elBonyurt", sender: self)
        } else {
         self.createAlertNetwork(title: "No hay conexión a Internet", message: "Verifica la conexión a Internet.")
        }
    }
    
    @IBAction func toMyStats(_ sender: Any) {
        if CheckNetwork.Connection(){
            performSegue(withIdentifier: "caritox", sender: self)
        } else {
            self.createAlertNetwork(title: "No hay conexión a Internet", message: "Verifica la conexión a Internet.")
        }
        
    }
    func updateUserData(){
        guard CheckNetwork.Connection() == true else {return}
        docRef.addSnapshotListener{(docSnapshot, error) in
            guard let docSnapshot = docSnapshot, docSnapshot.exists else {return}
            let data = docSnapshot.data()
            let name = data?["nombre"] as? String ?? ""
            let IMC = data?["IMC"] as? Double ?? 0.0
            let fat = data?["grasaCorporal"] as? Double ?? 0.0
            let weight = data?["peso"] as? Double ?? 0.0
            let maxHR = data?["maxHR"] as? Int ?? 0
            let lastName = data?["apellido"] as? String ?? ""
            let imagenUrl = data?["imagen"] as? String ?? ""
            do {
                let urlImage = URL(string: imagenUrl)
                let dataImage = try Data(contentsOf: urlImage!)
                let img = UIImage(data: dataImage)!
                if name != self.user.name || lastName != self.user.lastName || IMC != self.user.IMC || fat != self.user.fat || weight != self.user.weight || maxHR != self.user.maxHR{
                    self.user = User(name: name, lastName: lastName, weight: weight, fat: fat, IMC: IMC, maxHR: maxHR, photo: img)
                    self.saveUser()
                    self.showUserInfo()
                }
            }catch{
                print(error)
            }
            

        }
    }
    
    func showUserInfo(){
        let name = user.name ?? ""
        let lastName = user.lastName ?? ""
        self.nameLabel.text = "\(name) \(lastName)"
        self.weightLabel.text = "\(user.weight ?? 0) kg"
        self.fatLabel.text = "\(user.fat ?? 0)%"
        self.maxHRLabel.text = "\(user.maxHR ?? 0) rpm"
        self.IMCLabel.text = "\(user.IMC ?? 0)"
        self.profileImg.image = user.photo
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func saveUser(){
        let isSuccessfulSave = NSKeyedArchiver.archiveRootObject(user, toFile: User.ArchiveURL.path)
        if isSuccessfulSave{
            os_log("Exercises successfully saved", log: OSLog.default, type: .debug)
        } else {
            os_log("Failed to save exercises", log: OSLog.default, type: .error)
        }
    }
    
    private func loadUser()-> User?{
        return NSKeyedUnarchiver.unarchiveObject(withFile: User.ArchiveURL.path) as? User
    }
    
    func createAlertNetwork(title: String, message: String ){
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default , handler: { (action) in
            alert.dismiss(animated: true, completion: nil)
        }))
        
        self.present(alert, animated: true, completion: nil)
        
    }
    

    @IBAction func logout(_ sender: UIBarButtonItem) {
        UserDefaults.standard.removeObject(forKey: "userID")
        UserDefaults.standard.removeObject(forKey: "rol")
        let firebaseAuth = Auth.auth()
        do {
            try firebaseAuth.signOut()
            self.performSegue(withIdentifier: "logoutSegue", sender: self)
        } catch let signOutError as NSError {
            print ("Error signing out: %@", signOutError)
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
