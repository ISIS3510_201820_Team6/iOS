//
//  Plan.swift
//  gymAR
//
//  Created by user145541 on 10/4/18.
//  Copyright © 2018 user145541. All rights reserved.
//

import UIKit
import os.log

class Plan: NSObject, NSCoding {

    
    
    var routines: [Routine]
    var id: String?
    var semanas: Int?
    var diasPorSemana: Int?
    var type: String?
   
    //MARK: Archiving Paths
    
    static let DocumentsDirectory = FileManager().urls(for: .documentDirectory, in: .userDomainMask).first!
    static let ArchiveURL = DocumentsDirectory.appendingPathComponent("plans3")
    
    //MARK: Types
    struct PropertyKey{
        static let routines = "routines"
        static let semanas = "semanas"
        static let diasPorSemana = "diasPorSemana"
        static let id = "id"
        static let type = "type"
    }
    
    init(routines: [Routine], semanas: Int, diasPorSemana: Int, id: String, type: String) {
        self.routines = routines
        self.semanas = semanas
        self.diasPorSemana = diasPorSemana
        self.id = id
        self.type = type
    }
    // MARK: NSCoding
    func encode(with aCoder: NSCoder) {
        aCoder.encode(routines, forKey: PropertyKey.routines)
        aCoder.encode(semanas, forKey: PropertyKey.semanas)
        aCoder.encode(diasPorSemana, forKey: PropertyKey.diasPorSemana)
        aCoder.encode(id, forKey: PropertyKey.id)
        aCoder.encode(type, forKey: PropertyKey.type)
    }
    
    required convenience init?(coder aDecoder: NSCoder) {
        let routines = aDecoder.decodeObject(forKey: PropertyKey.routines) as! [Routine]
        let semanas = aDecoder.decodeObject(forKey: PropertyKey.semanas) as! Int
        let diasPorSemana = aDecoder.decodeObject(forKey: PropertyKey.diasPorSemana) as! Int
        let id = aDecoder.decodeObject(forKey: PropertyKey.id) as! String
        let type = aDecoder.decodeObject(forKey: PropertyKey.type) as! String
        self.init(routines: routines, semanas: semanas, diasPorSemana: diasPorSemana, id: id, type: type)
    }
}
