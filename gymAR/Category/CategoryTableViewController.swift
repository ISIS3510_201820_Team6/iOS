//
//  CategoryTableViewController.swift
//  gymAR
//
//  Created by Juan Diego González on 11/5/18.
//  Copyright © 2018 user145541. All rights reserved.
//

import UIKit
import Firebase
import os.log

class CategoryTableViewController: UITableViewController {
    
    var numberRows = 0
    var collectionRef : CollectionReference!
    var categories = [Category]()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        DispatchQueue.global(qos: .background).async {
            self.loadData()
        }
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return categories.count
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70.0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cellIdentifier = "categoryCell"
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? CategoryTableViewCell  else {
            fatalError("The dequeued cell is not an instance of CategoryTableViewCell.")
        }
        
        let category = categories[indexPath.row]
        cell.nombreLabel.text = category.name
        cell.elIcono.image = category.icon
        return cell
    }
    
    func loadData(){
        if CheckNetwork.Connection(){
            collectionRef = Firestore.firestore().collection("categorias")
            collectionRef.getDocuments{ (querySnapshot, err) in
                if let err = err {
                    print("Error getting documents:\(err)")
                } else {
                    for document in querySnapshot!.documents{
                        let data = document.data()
                        let nombre = data["nombre"] as? String ?? ""
                        let iconUrl = data["iconoIOS"] as? String ?? ""
                        DispatchQueue.global().async {
                            do{
                                let urlIcon = URL(string: iconUrl)
                                let dataIcon = try Data(contentsOf: urlIcon!)
                                let icon = UIImage(data: dataIcon)!
                                print(nombre)
                                let catObject = Category(name: nombre, icon: icon)
                                self.categories.append(catObject!)
                                DispatchQueue.main.sync {
                                    self.tableView.reloadData()
                                }
                            } catch {
                                print(error)
                            }
                        }

//                        self.tableView.reloadData()
                      
                    }
                   
                    if self.categories.count > 0{
                        // Salvar categoria
                        self.saveData()
                    }
                }
            }
        }else{
            createAlertNetwork(title: "Your phone has no Internet connection", message: "In order to help you out in your workout we will display some exercises that where previously loaded.")
            
            if let losQueSon = self.loadCat(){
                if categories.count == 0 {
                    for cat in losQueSon{
                        let nombre = cat.name
                        let icono = cat.icon
                        let catObj = Category(name: nombre, icon: icono!)
                        self.categories.append(catObj!)
//                        self.tableView.reloadData()
                    }
                }
            }
            
        }
        
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?){
        if CheckNetwork.Connection(){
            if segue.identifier == "segueAEjercicio"{
                if let indexPath = tableView.indexPathForSelectedRow{
                    let destVC = segue.destination as! ExerciseTableViewController
                    destVC.nombreCategoria = categories[indexPath.row].name
                }
            }
        } else {
            self.createAlertNetwork(title: "No hay conexión a Internet", message: "Verifica la conexión a Internet.")
        }
       
    }
    
    private func saveData(){
        let isSuccessfulSave = NSKeyedArchiver.archiveRootObject(categories, toFile: Category.ArchiveURL.path)
        if isSuccessfulSave{
            os_log("Exercises successfully saved", log: OSLog.default, type: .debug)
        } else {
            os_log("Failed to save exercises", log: OSLog.default, type: .error)
        }
    }
    
    private func loadCat()-> [Category]?{
        return NSKeyedUnarchiver.unarchiveObject(withFile: Category.ArchiveURL.path) as? [Category]
    }
    
    func createAlertNetwork(title: String, message: String ){
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default , handler: { (action) in
            alert.dismiss(animated: true, completion: nil)
        }))
        
        self.present(alert, animated: true, completion: nil)
        
    }
    
    
    /*
     // Override to support conditional editing of the table view.
     override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the specified item to be editable.
     return true
     }
     */
    
    /*
     // Override to support editing the table view.
     override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
     if editingStyle == .delete {
     // Delete the row from the data source
     tableView.deleteRows(at: [indexPath], with: .fade)
     } else if editingStyle == .insert {
     // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
     }
     }
     */
    
    /*
     // Override to support rearranging the table view.
     override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
