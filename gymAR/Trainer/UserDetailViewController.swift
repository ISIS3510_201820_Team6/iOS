//
//  UserDetailViewController.swift
//  gymAR
//
//  Created by Juan Diego González on 12/8/18.
//  Copyright © 2018 user145541. All rights reserved.
//

import UIKit

class UserDetailViewController: UIViewController {

    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var condiciones: UITextView!
    @IBOutlet weak var elEjercicio: UILabel!
    var nameLabel : String!
    var imagenChevere : UIImage!
    var condicionesText: String!
    var ejercicio: String!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        image.layer.cornerRadius = image.frame.size.width/2
        
        if let nameLabel = self.nameLabel{
            self.name.text = nameLabel
        }
        if let condicionesText = self.condicionesText{
            self.condiciones.text = condicionesText
        }
        if let imagenChevere = self.imagenChevere{
            do{
                self.image.image = imagenChevere
            } catch{
                print(error)
            }
        }
        if let ejercicio = self.ejercicio{
            self.elEjercicio.text = ejercicio
        }
        
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
