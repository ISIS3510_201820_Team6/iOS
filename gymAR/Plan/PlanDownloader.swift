//
//  PlanDownloader.swift
//  gymAR
//
//  Created by user145541 on 10/28/18.
//  Copyright © 2018 user145541. All rights reserved.
//

import UIKit
import Firebase
import os.log

class PlanDownloader{
    
    func downloadExercises(dataRoutine: [String: Any]?, completion: @escaping ([RoutineExercise]) -> Void){
        var arrExcercises = [RoutineExercise]()
        let routine = dataRoutine?["ejercicios"] as! NSArray
        var cont = 0
        var currentExercise: NSDictionary
        var numRepPorSerie: Int
        var ref: DocumentReference
        var numSeries: Int
        for exercise in routine{
            currentExercise = exercise as! NSDictionary
            ref = currentExercise["ejercicio"] as! DocumentReference
            numRepPorSerie = currentExercise["numeroRepeticionesPorSerie"] as! Int
            numSeries = currentExercise["numeroSeries"] as! Int
            self.downloadExercisesData(ref: ref, numSeries: numSeries, numRepPorSerie: numRepPorSerie){ rutEx in
                arrExcercises.append(rutEx)
                cont += 1
                if cont == routine.count{
                    completion(arrExcercises)
                }
            }
        }
    }
    
    func downloadRoutines(routines: [DocumentReference], completion: @escaping ([Routine]) -> Void){
        var arrRoutines = [Routine]()
        var count = 0
        for idRoutine in routines{
            idRoutine.addSnapshotListener{(docSnapshot2, error) in
                guard let docSnapshot2 = docSnapshot2, docSnapshot2.exists else {
                    completion(arrRoutines)
                    return
                }
                let dataRoutine = docSnapshot2.data()
                let name = dataRoutine?["nombre"] as! String
                self.downloadExercises(dataRoutine: dataRoutine){ arrExcercises in
                    let exercises = arrExcercises
                    let routine = Routine(exercises: exercises, name: name)
                    arrRoutines.append(routine)
                    count += 1
                    if count == routines.count{
                        completion(arrRoutines)
                    }
                }
            }
        }
    }
    
    
    func downloadExercisesData(ref: DocumentReference, numSeries: Int, numRepPorSerie: Int, completion: @escaping (RoutineExercise) -> Void){
        var rutEx: RoutineExercise!
        ref.addSnapshotListener({(docSnapshot3, error) in
            guard let docSnapshot3 = docSnapshot3, docSnapshot3.exists else {
                completion(rutEx)
                return
            }
            let dataExercise = docSnapshot3.data()
            let exName = dataExercise!["nombre"] as! String
            let video = dataExercise!["video"] as! String
            let description = dataExercise!["descripcion"] as! String
            let imgUrl = dataExercise!["foto"] as? String ?? ""
            let iconUrl = dataExercise!["iconoIOS"] as? String ?? ""
            do{
                let urlImage = URL(string: imgUrl)
                let urlIcon = URL(string: iconUrl)
                let dataImage = try Data(contentsOf: urlImage!)
                let dataIcon = try Data(contentsOf: urlIcon!)
                let img = UIImage(data: dataImage)!
                let icon = UIImage(data: dataIcon)!
                let rutExercise = RoutineExercise(name: exName, urlVideo: video, description: description, photo: img, icon: icon, series: numSeries, reps: numRepPorSerie)
                rutEx = rutExercise
                completion(rutEx)
            }catch{
                print(error)
            }
        })
        
    }
    
    func downloadPlan(queryRef: Query, completion: @escaping (Plan) -> Void){
        queryRef.addSnapshotListener{ querySnapshot, error in
            guard let snapshot = querySnapshot else {
                print("Error fetching snapshots: \(error!)")
                return
            }
            let data = querySnapshot?.documents[0].data()
            let plan = data?["plan"] as! DocumentReference
            var elPlan: Plan!
            let id = plan.documentID
            plan.addSnapshotListener{(docSnapshot, error) in
                guard let docSnapshot = docSnapshot, docSnapshot.exists else {
                    completion(elPlan)
                    return
                }
                let data = docSnapshot.data()
                let type = data?["nivel"] as? String ?? ""
                let daysPerWeek = data?["diasSemana"] as? Int ?? 0
                let weeks = data?["semanas"] as? Int ?? 0
                let routines = data?["rutinas"] as? [DocumentReference] ?? []
                self.downloadRoutines(routines: routines){ arrRoutines in
                    elPlan = Plan(routines: arrRoutines, semanas: weeks, diasPorSemana: daysPerWeek, id: id, type: type)
                    completion(elPlan)
                }
                // Do any additional setup after loading the view.
            }
        }
    }
}
