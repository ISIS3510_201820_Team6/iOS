//
//  User.swift
//  gymAR
//
//  Created by user145541 on 10/2/18.
//  Copyright © 2018 user145541. All rights reserved.
//

import UIKit
import os.log

class User: NSObject, NSCoding {
    var name: String?
    var photo: UIImage
    var weight: Double?
    var fat: Double?
    var IMC: Double?
    var maxHR: Int?
    var lastName: String?
    //var sCondition: SpecialCondition
    //var evolution: PhysicalEvolution
    
    static let DocumentsDirectory = FileManager().urls(for: .documentDirectory, in: .userDomainMask).first!
    static let ArchiveURL = DocumentsDirectory.appendingPathComponent("users")
    
    struct PropertyKey{
        static let name = "name"
        static let weight = "weight"
        static let fat = "fat"
        static let IMC = "IMC"
        static let maxHR = "maxHR"
        static let lastName = "lastName"
        static let photo = "photo"
    }
    
    init(name: String, lastName: String, weight: Double, fat: Double, IMC: Double, maxHR: Int, photo: UIImage) {
        self.name = name
        self.photo = photo
        self.weight = weight
        self.fat = fat
        self.IMC = IMC
        self.maxHR = maxHR
        self.lastName = lastName
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(name, forKey: PropertyKey.name)
        aCoder.encode(lastName, forKey: PropertyKey.lastName)
        aCoder.encode(weight, forKey: PropertyKey.weight)
        aCoder.encode(fat, forKey: PropertyKey.fat)
        aCoder.encode(IMC, forKey: PropertyKey.IMC)
        aCoder.encode(maxHR, forKey: PropertyKey.maxHR)
        aCoder.encode(photo, forKey: PropertyKey.photo)
    }
    
    required convenience init?(coder aDecoder: NSCoder) {
        let name = aDecoder.decodeObject(forKey: PropertyKey.name) as! String
        let lastName = aDecoder.decodeObject(forKey: PropertyKey.lastName) as! String
        let weight = aDecoder.decodeObject(forKey: PropertyKey.weight) as! Double
        let fat = aDecoder.decodeObject(forKey: PropertyKey.fat) as! Double
        let IMC = aDecoder.decodeObject(forKey: PropertyKey.IMC) as! Double
        let maxHR = aDecoder.decodeObject(forKey: PropertyKey.maxHR) as! Int
        let photo = aDecoder.decodeObject(forKey: PropertyKey.photo) as! UIImage
        self.init(name: name, lastName: lastName, weight: weight, fat: fat, IMC: IMC, maxHR: maxHR, photo: photo)
    }
}
