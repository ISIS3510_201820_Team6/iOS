//
//  TrainerTableViewController.swift
//  gymAR
//
//  Created by NICOLAS EDUARDO CABRERA VENEGAS on 10/30/18.
//  Copyright © 2018 user145541. All rights reserved.
//

import UIKit
import Firebase

class TrainerTableViewController: UITableViewController {
    var queryRef: Query!
    var assignations: [Assignation]!
    var trainer: Trainer!
    var tid: String!
    var counter: Int!
    override func viewDidLoad() {
        super.viewDidLoad()
        counter = 0
        tid = UserDefaults.standard.value(forKey: "userID") as? String ?? ""
        print(tid!)
        print("bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb")
        queryRef = Firestore.firestore().collection("trainerUser").whereField("tId", isEqualTo: tid!).whereField("realizada", isEqualTo: false).order(by: "fechaSolicitud", descending: false)
        if CheckNetwork.Connection(){
            DispatchQueue.global(qos: .background).async{
                self.downLoadPetition()
            }
        }else{
            createAlertNetwork(title: "Tu telefono no tiene conexión a internet", message: "Las llamadas no se podran mostrar hasta que tengas conexión")
        }
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }
    
    func downLoadTrainer(completion: @escaping (Trainer) -> Void){
        let docRef = Firestore.firestore().document("usuarios/\(tid!)")
        var trainer: Trainer!
        docRef.addSnapshotListener { (docSnapshot, error) in
            guard let docSnapshot = docSnapshot, docSnapshot.exists else {
                completion(trainer)
                return
            }
            let tData = docSnapshot.data()
            let name = tData?["nombre"] as? String ?? ""
            let zone = tData?["zona"] as? String ?? ""
            trainer = Trainer(name: name, zone: zone)
            completion(trainer)
        }
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        if assignations == nil{
            return 0
        } else{
            return assignations.count
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "clientAssignationCell", for: indexPath) as? TrainerTableViewCell else {
            fatalError("The dequeued cell is not an instance of TrainerTableViewCell.")
        }
        let assigned = assignations[indexPath.row]
        cell.clientImage.image = assigned.img
        cell.clientLocationLabel.text = assigned.exercise
        cell.clientNameLabel.text = assigned.user
        cell.dateLabel.text = assigned.fecha
     // Configure the cell...
     
     return cell
     }
    
    func downLoadPetition(){
        var assigns = [Assignation]()
        queryRef.addSnapshotListener { (querySnapshot, error) in
            guard let snapshot = querySnapshot else {
                print("Error fetching snapshots: \(error!)")
                return
            }
            var assignation: Assignation!
            var img: UIImage!
            var id: String
            var user: String
            var exercise: String
            var ts: Date
            var formatter: DateFormatter
            var date: String
            var imgUrl: String
            var data: [String: Any]
            var lasCondiciones : String
            for document in querySnapshot!.documents{
                data = document.data()
                id = document.documentID
                user = data["cliente"] as! String
                exercise = data["ejercicio"] as! String
                ts = data["fechaSolicitud"] as! Date
                lasCondiciones = data["condiciones"] as! String
                formatter = DateFormatter()
                formatter.dateFormat = "hh:mm"
                date = formatter.string(from: ts)
                imgUrl = data["imagen"] as! String
                do{
                    let urlImage = URL(string: imgUrl)
                    let dataImage = try Data(contentsOf: urlImage!)
                    img = UIImage(data: dataImage)!
                }catch{
                    print(error)
                }
                assignation = Assignation(user: user, exercise: exercise, img: img, fecha: date, docId: id, condiciones: lasCondiciones)
                        assigns.append(assignation)
                        self.counter += 1
                        if self.counter == querySnapshot!.documents.count{
                            self.counter = 0
                            self.assignations = assigns
                            assigns.removeAll()
//                            print("ccccccccccccccccccccc")
                            print(self.assignations.count)
                            DispatchQueue.main.async{
                                self.tableView.reloadData()
                            }
                        }
                    }
                }
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let docId = self.assignations[indexPath.row].docId
            self.assignations.remove(at: indexPath.row)
            let docRef = Firestore.firestore().document("trainerUser/\(docId)")
            docRef.updateData([
                "realizada": true
            ]) { err in
                if let err = err {
                    print("Error updating document: \(err)")
                } else {
                    print("Document successfully updated")
                }
            }
            tableView.deleteRows(at: [indexPath], with: .fade)
            
            // Delete the row from the data source
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    
    func createAlertNetwork(title: String, message: String ){
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default , handler: { (action) in
            alert.dismiss(animated: true, completion: nil)
        }))
        
        self.present(alert, animated: true, completion: nil)
        
    }
 
    @IBAction func logout(_ sender: UIBarButtonItem) {
        UserDefaults.standard.removeObject(forKey: "userId")
        UserDefaults.standard.removeObject(forKey: "rol")
        let firebaseAuth = Auth.auth()
        do {
            try firebaseAuth.signOut()
            self.performSegue(withIdentifier: "logoutTrainer", sender: self)
        } catch let signOutError as NSError {
            print ("Error signing out: %@", signOutError)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?){
        if CheckNetwork.Connection(){
            if segue.identifier == "elGranVaron"{
                if let indexPath = tableView.indexPathForSelectedRow{
                    let destVC = segue.destination as! UserDetailViewController
                    destVC.nameLabel = assignations[indexPath.row].user
                    destVC.condicionesText = assignations[indexPath.row].condiciones
                    destVC.ejercicio = assignations[indexPath.row].exercise
                    destVC.imagenChevere = assignations[indexPath.row].img
                }
            }
        } else {
            self.createAlertNetwork(title: "No hay conexión a Internet", message: "Verifica la conexión a Internet.")
        }
        
    }
    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*v
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
