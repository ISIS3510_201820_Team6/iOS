//
//  LineChartViewController.swift
//  gymAR
//
//  Created by Juan Diego González on 11/3/18.
//  Copyright © 2018 user145541. All rights reserved.
//

import UIKit
import Charts
import Firebase
import os.log

class LineChartViewController: UIViewController {

    var months: [String]!
    var losDatazos : Query!
    var peso = [Double]()
    var grasa = [Double]()
    var perimetro = [Double]()
    var fecha = [String]()
    var staticsValues : StatisticValue!
    
    

    @IBOutlet weak var lineChartViewPerimetro: LineChartView!
    @IBOutlet weak var lineChartViewGrasa: LineChartView!
    @IBOutlet weak var lineChartView: LineChartView!
    override func viewDidLoad() {
        
        
        super.viewDidLoad()
        self.navigationController!.navigationBar.topItem!.title = "Atras"
        let myId = UserDefaults.standard.value(forKey: "userID") as? String ?? ""
        print(myId)
        losDatazos = Firestore.firestore().collection("statistics").whereField("userId", isEqualTo: myId).order(by: "fecha")
        
        DispatchQueue.global(qos: .background).async {
            self.traerLosDatos()
        }
    
        let ll = ChartLimitLine(limit: 90, label: "Meta")
        lineChartView.rightAxis.addLimitLine(ll)
        // Do any additional setup after loading the view.
    }
    
    func setChartPeso(dataPoints: [String], values:[Double]){
        var dataEntries: [ChartDataEntry] = []
        let formatter = BarChartFormatter()
        formatter.setValues(values:dataPoints)
        let xaxis:XAxis = XAxis()
        lineChartView.noDataText = "No data."
        
        for i in 0..<dataPoints.count{
            let dataEntry = ChartDataEntry(x: Double(i), y: values[i])
            dataEntries.append(dataEntry)
        }
    
        
        let lineChartDataSet = LineChartDataSet(values: dataEntries, label: "Peso")
        let lineChartData = LineChartData(dataSet: lineChartDataSet)
        print(lineChartData)
        

        xaxis.valueFormatter = formatter
        lineChartView.xAxis.labelPosition = .bottom
        lineChartView.xAxis.drawGridLinesEnabled = false
        lineChartView.xAxis.valueFormatter = xaxis.valueFormatter
        lineChartView.xAxis.granularityEnabled = true
        lineChartView.xAxis.granularity = 1.0
        lineChartView.xAxis.decimals = 0
        lineChartView.chartDescription?.enabled = false
        lineChartView.legend.enabled = true
        lineChartView.rightAxis.enabled = false
       

        lineChartView.data = lineChartData
        
        lineChartView.data = lineChartData

    }
    
    func setChartGrasa(dataPoints: [String], values:[Double]){
        var dataEntries: [ChartDataEntry] = []
        let formatter = BarChartFormatter()
        formatter.setValues(values:dataPoints)
        let xaxis:XAxis = XAxis()
        lineChartViewGrasa.noDataText = "No data."
        
        for i in 0..<dataPoints.count{
            let dataEntry = ChartDataEntry(x: Double(i), y: values[i])
            dataEntries.append(dataEntry)
        }
        
        
        let lineChartDataSet = LineChartDataSet(values: dataEntries, label: "Grasa corporal")
        let lineChartData = LineChartData(dataSet: lineChartDataSet)
        print(lineChartData)
        
        let ll = ChartLimitLine(limit: 90, label: "Meta")
        lineChartViewGrasa.rightAxis.addLimitLine(ll)
        
        xaxis.valueFormatter = formatter
        lineChartViewGrasa.xAxis.labelPosition = .bottom
        lineChartViewGrasa.xAxis.drawGridLinesEnabled = false
        lineChartViewGrasa.xAxis.valueFormatter = xaxis.valueFormatter
        lineChartViewGrasa.xAxis.granularityEnabled = true
        lineChartViewGrasa.xAxis.granularity = 1.0
        lineChartViewGrasa.xAxis.decimals = 0
        lineChartViewGrasa.chartDescription?.enabled = false
        lineChartViewGrasa.legend.enabled = true
        lineChartViewGrasa.rightAxis.enabled = false
        
        lineChartViewGrasa.data = lineChartData

    }
    
    func setChartPerimetro(dataPoints: [String], values:[Double]){
        var dataEntries: [ChartDataEntry] = []
        let formatter = BarChartFormatter()
        formatter.setValues(values:dataPoints)
        let xaxis:XAxis = XAxis()
        lineChartViewPerimetro.noDataText = "No data."
        
        for i in 0..<dataPoints.count{
            let dataEntry = ChartDataEntry(x: Double(i), y: values[i])
            dataEntries.append(dataEntry)
        }
        
        
        let lineChartDataSet = LineChartDataSet(values: dataEntries, label: "Perimetro abdominal")
        let lineChartData = LineChartData(dataSet: lineChartDataSet)
        print(lineChartData)
        
        let ll = ChartLimitLine(limit: 90, label: "Meta")
        lineChartViewPerimetro.rightAxis.addLimitLine(ll)
        
        xaxis.valueFormatter = formatter
        lineChartViewPerimetro.xAxis.labelPosition = .bottom
        lineChartViewPerimetro.xAxis.drawGridLinesEnabled = false
        lineChartViewPerimetro.xAxis.valueFormatter = xaxis.valueFormatter
        lineChartViewPerimetro.xAxis.granularityEnabled = true
        lineChartViewPerimetro.xAxis.granularity = 1.0
        lineChartViewPerimetro.xAxis.decimals = 0
        lineChartViewPerimetro.chartDescription?.enabled = false
        lineChartViewPerimetro.legend.enabled = true
        lineChartViewPerimetro.rightAxis.enabled = false
        
        lineChartViewPerimetro.data = lineChartData

    }
    
    
    func traerLosDatos(){
        
        if CheckNetwork.Connection(){
            losDatazos.addSnapshotListener{ (querySnapshot, error) in
                guard let snapshot = querySnapshot else {
                    print("Error fetching snapshots: \(error!)")
                    return
                }
                print("hola"
                )
                for document in querySnapshot!.documents{
                    let data = document.data()
                    let elPeso = data["peso"] as! Double
                    let laGrasita = data["grasa"] as! Double
                    let elPerimetro = data["perimetro"] as! Double
                    let fechita = data["fecha"] as! Date
                    
                    // Se convierte la fecha a string
                    
                    let formatter = DateFormatter()
                    formatter.dateFormat = "LLLL"
                    let month = formatter.string(from: fechita)
                    
                    self.peso.append(elPeso)
                    self.grasa.append(laGrasita)
                    self.perimetro.append(elPerimetro)
                    self.fecha.append(month)
                    
                }
                self.setChartPeso(dataPoints: self.fecha, values: self.peso)
                self.setChartGrasa(dataPoints: self.fecha, values: self.grasa)
                self.setChartPerimetro(dataPoints: self.fecha, values: self.perimetro)
                self.staticsValues = StatisticValue(peso: self.peso, grasa: self.grasa, perimetro: self.perimetro, fecha: self.fecha)
                if self.staticsValues.grasa.count > 0 {
                    self.saveData()
                }
                
            }

            
        }else{
             createAlertNetwork(title: "No estás conectado a una red de Internet", message: "Los datos que aparecen en pantalla pueden que no sean los últimos")
            if let losQueSon = self.loadData(){
                if peso.count == 0 {
                    self.peso = losQueSon.peso
                    self.grasa = losQueSon.grasa
                    self.perimetro = losQueSon.perimetro
                    self.fecha = losQueSon.fecha
                    
                    self.setChartPeso(dataPoints: self.fecha, values: self.peso)
                    self.setChartGrasa(dataPoints: self.fecha, values: self.grasa)
                    self.setChartPerimetro(dataPoints: self.fecha, values: self.perimetro)
                }
            }
        }

    }
    
    @objc(LineChartFormatter)
    public class BarChartFormatter: NSObject, IAxisValueFormatter {
        var names = [String]()
        
        public func stringForValue(_ value: Double, axis: AxisBase?) -> String {
            return names[Int(value)]
        }
        
        public func setValues(values: [String]) {
            self.names = values
        }
    }
    
    private func saveData(){
        let isSuccessfulSave = NSKeyedArchiver.archiveRootObject(staticsValues, toFile: StatisticValue.ArchiveURL.path)
        if isSuccessfulSave{
            os_log("Exercises successfully saved", log: OSLog.default, type: .debug)
        } else {
            os_log("Failed to save exercises", log: OSLog.default, type: .error)
        }
        
    }
    
    private func loadData()-> StatisticValue?{
        return NSKeyedUnarchiver.unarchiveObject(withFile: StatisticValue.ArchiveURL.path) as? StatisticValue
    }
    
    func createAlertNetwork(title: String, message: String ){
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default , handler: { (action) in
            alert.dismiss(animated: true, completion: nil)
        }))
        
        self.present(alert, animated: true, completion: nil)
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
