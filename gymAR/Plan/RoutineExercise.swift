//
//  RutineExercise.swift
//  gymAR
//
//  Created by user145541 on 10/5/18.
//  Copyright © 2018 user145541. All rights reserved.
//

import UIKit
import os.log

class RoutineExercise: NSObject, NSCoding {
    
    var name : String
    var photo : UIImage?
    var icon : UIImage?
    var urlVideo : String?
    var arImage : String?
    var descrip : String?
    var series: Int?
    var reps: Int?
    
    static let DocumentsDirectory = FileManager().urls(for: .documentDirectory, in: .userDomainMask).first!
    static let ArchiveURL = DocumentsDirectory.appendingPathComponent("routineExercises2")
    
    struct PropertyKey{
        static let name = "name"
        static let urlVideo = "urlVideo"
        static let arImage = "arImage"
        static let descrip = "descrip"
        static let photo = "photo"
        static let icon = "icon"
        static let series = "series"
        static let reps = "reps"
    }
    
    init(name: String, urlVideo: String, description: String, photo: UIImage, icon: UIImage,  series: Int, reps: Int) {
        self.name = name
        self.photo = photo
        self.urlVideo = urlVideo
        self.descrip = description
        self.icon = icon
        self.series = series
        self.reps = reps
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(name, forKey: PropertyKey.name)
        aCoder.encode(urlVideo, forKey: PropertyKey.urlVideo)
        aCoder.encode(arImage, forKey: PropertyKey.arImage)
        aCoder.encode(descrip, forKey: PropertyKey.descrip)
        aCoder.encode(photo, forKey: PropertyKey.photo)
        aCoder.encode(icon, forKey: PropertyKey.icon)
        aCoder.encode(series, forKey: PropertyKey.series)
        aCoder.encode(reps, forKey: PropertyKey.reps)
    }
    
    required convenience init?(coder aDecoder: NSCoder) {
        guard let name = aDecoder.decodeObject(forKey: PropertyKey.name) as? String else {
            os_log("Unable to decode the name for a Exercise object.", log: OSLog.default, type: .debug)
            return nil
        }
        let urlVideo = aDecoder.decodeObject(forKey: PropertyKey.urlVideo) as! String
        let descrip = aDecoder.decodeObject(forKey: PropertyKey.descrip) as! String
        let photo = aDecoder.decodeObject(forKey: PropertyKey.photo) as! UIImage
        let icon = aDecoder.decodeObject(forKey: PropertyKey.icon) as! UIImage
        let reps = aDecoder.decodeObject(forKey: PropertyKey.reps) as! Int
        let series = aDecoder.decodeObject(forKey: PropertyKey.series) as! Int
        self.init(name: name, urlVideo: urlVideo, description: descrip, photo: photo, icon: icon, series: series, reps: reps)
    }
    
}
