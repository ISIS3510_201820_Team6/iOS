//
//  TabBarController.swift
//  gymAR
//
//  Created by Juan Diego González on 12/6/18.
//  Copyright © 2018 user145541. All rights reserved.
//

import UIKit

class TabBarController: UITabBarController {

    @IBOutlet weak var tab: UITabBar!
    var laSabrosonga: Bool!
    override func viewDidLoad() {
        super.viewDidLoad()
        laSabrosonga = false
        Timer.scheduledTimer(withTimeInterval: 0.5, repeats: true) { (timer) in
            self.handleButtons()
        }
        handleButtons();
        // Do any additional setup after loading the view.
        
    }
    
    func handleButtons(){
        DispatchQueue.global(qos: .background).async {
            if !CheckNetwork.Connection(){
                if self.laSabrosonga == false{
                    self.createAlertNetwork(title: "No hay conexión a Internet", message: "Verifica la conexión a Internet.")
                    self.laSabrosonga = true
                }
                let saved = UserDefaults.standard.value(forKey: "savedPlan") as? Bool ?? false
                if !saved && self.tabBar.items?[1].isEnabled == true {
                    DispatchQueue.main.async {
                        self.tabBar.items?[1].isEnabled = false
                    }
                }
                if self.tabBar.items?[2].isEnabled == true{
                    DispatchQueue.main.async {
                        self.tabBar.items?[2].isEnabled = false
                    }
                }
            } else if self.tabBar.items?[1].isEnabled == false{
                
                DispatchQueue.main.async {
                    self.tabBar.items?[1].isEnabled = true
                }
            } else if self.tabBar.items?[2].isEnabled == false{
                self.laSabrosonga = false
                DispatchQueue.main.async {
                    self.tabBar.items?[2].isEnabled = true
                }
            }
        }
    }
    
    
    func createAlertNetwork(title: String, message: String ){
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default , handler: { (action) in
            alert.dismiss(animated: true, completion: nil)
        }))
        
        self.present(alert, animated: true, completion: nil)
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
