//
//  VideoViewController.swift
//  gymAR
//
//  Created by Juan Diego González on 10/6/18.
//  Copyright © 2018 user145541. All rights reserved.
//

import UIKit

class VideoViewController: UIViewController {
    
    @IBOutlet weak var elVideo: UIWebView!
    
    @IBOutlet weak var noInternetImage: UIImageView!
    
    var name : String?
    var video : String? 
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getVideo(videoCode: self.video!)
    }
    
    func getVideo(videoCode: String){
        if CheckNetwork.Connection(){
            noInternetImage.alpha = 0
            let url = URL(string: "https://www.youtube.com/embed/\(videoCode)")
            elVideo.loadRequest(URLRequest(url: url!))
        } else {
            self.createAlertNetwork(title: "No Internet Connection", message: "Sorry! We can not display the video at this moment. Please Check your internet connection")
            noInternetImage.alpha = 1
            
        }

        
    }
    func createAlertNetwork(title: String, message: String ){
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default , handler: { (action) in
            alert.dismiss(animated: true, completion: nil)
        }))
        
        self.present(alert, animated: true, completion: nil)
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
