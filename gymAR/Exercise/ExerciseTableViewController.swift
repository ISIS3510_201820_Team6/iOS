//
//  ExerciseTableViewController.swift
//  gymAR
//
//  Created by Juan Diego González on 10/3/18.
//  Copyright © 2018 user145541. All rights reserved.
//

import UIKit
import Firebase
import os.log

class ExerciseTableViewController: UITableViewController {
    // MARK: Properties
    var nombreCategoria : String!
    var numberRows = 0
    var collectionRef: CollectionReference!
    var elQuery: Query!
    var exercises = [Exercise]()
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("\(nombreCategoria)")
        
        elQuery = Firestore.firestore().collection("ejercicios").whereField("nombreCategoria", isEqualTo: nombreCategoria)
        DispatchQueue.global(qos: .background).async {
            self.loadData()
        }
        
    }
    
//    override func viewWillAppear(_ animated: Bool) {
//        super.viewWillAppear(animated)
//        print("\(nombreCategoria)")
//        elQuery = Firestore.firestore().collection("ejercicios").whereField("nombreCategoria", isEqualTo: nombreCategoria)
//        loadData()
//
//    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print("\(exercises.count)")
        return exercises.count
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70.0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // Table view cells are reused and should be dequeued using a cell identifier.
        let cellIdentifier = "exerciseCell"
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? ExerciseTableViewCell  else {
            fatalError("The dequeued cell is not an instance of ExerciseTableViewCell.")
        }
        
        // Fetches the appropriate meal for the data source layout.
        let exercise = exercises[indexPath.row]
        print()
        cell.imageIcon.image = exercise.icon
        cell.nameLabel.text = exercise.name
        return cell
    }
    
    
    func loadData(){
        if CheckNetwork.Connection() {
            print("Cbro")
            elQuery.addSnapshotListener{ (querySnapshot, err) in
                if let err = err{
                    print("Error getting documents: \(err)")
                }else{
                    for document in querySnapshot!.documents{
                        let data = document.data()
                        let nombre = data["nombre"] as? String ?? ""
                        let nUrlVideo = data["video"] as? String ?? ""
                        let descripcion = data["descripcion"] as? String ?? ""
                        let imgUrl = data["foto"] as? String ?? ""
                        let iconUrl = data["iconoIOS"] as? String ?? ""
                        let zone = data["zona"] as? String ?? ""
                       
                        DispatchQueue.global().async {
                            do {
                                let urlImage = URL(string: imgUrl)
                                let urlIcon = URL(string: iconUrl)
                                let dataImage = try Data(contentsOf: urlImage!)
                                let dataIcon = try Data(contentsOf: urlIcon!)
                                let img = UIImage(data: dataImage)!
                                let icon = UIImage(data: dataIcon)!
                                let exerciseObj = Exercise(name: nombre, urlVideo: nUrlVideo, description: descripcion, photo: img, icon: icon, zone: zone)
                                self.exercises.append(exerciseObj!)

                                DispatchQueue.main.sync{
                                    self.tableView.reloadData()
                                }
                            }
                            catch{
                                print(error)
                            }
                        }
                        
                        
//                        self.exercises.append(exerciseObj!)
                    }


                    if self.exercises.count > 0 {
                        self.saveExercise()
                    }
                }
            }
        } else {
            createAlertNetwork(title: "No tienes conexión a Internet", message: "Para que puedas realizar tu entrenamiento, te mostramos una lista de ejercicios.")
            
            if let losQueSon = self.loadExercises(){
                print("hola")
                print("\(losQueSon.count)")
                if exercises.count == 0{
                    for exe in losQueSon{
                        let nombre = exe.name
                        let nUrlVideo = exe.urlVideo ?? ""
                        let descripcion = exe.descrip ?? ""
                        let img = exe.photo
                        let icon = exe.icon
                        let zone = exe.zone as? String ?? ""
                        let exeObject = Exercise(name: nombre, urlVideo: nUrlVideo, description: descripcion, photo: img!, icon: icon!, zone: zone)
                        self.exercises.append(exeObject!)
                        self.tableView.reloadData()
                    }
                }
            }
        }
        
        
        
    }
    // MARK: Navegation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let photo2 = UIImage(named: "myBodyImg")
        if CheckNetwork.Connection(){
            if segue.identifier == "segueDetail"{
                if let indexPath = tableView.indexPathForSelectedRow{
                    let destVC = segue.destination as! ExerciseViewController
                    destVC.name = exercises[indexPath.row].name
                    destVC.desc = exercises[indexPath.row].descrip
                    destVC.image = exercises[indexPath.row].photo
                    destVC.videoUrl = exercises[indexPath.row].urlVideo
                    destVC.zona = exercises[indexPath.row].zone
                    print("\(exercises[indexPath.row].description)")
                }
            }
        } else{
            self.createAlertNetwork(title: "No hay conexión a Internet", message: "Verifica la conexión a Internet.")
        }
        
    }
    
    //MARK: Private Methods
    private func saveExercise(){
        let isSuccessfulSave = NSKeyedArchiver.archiveRootObject(exercises, toFile: Exercise.ArchiveURL.path)
        if isSuccessfulSave{
            os_log("Exercises successfully saved", log: OSLog.default, type: .debug)
        } else {
            os_log("Failed to save exercises", log: OSLog.default, type: .error)
        }
    }
    
    private func loadExercises()-> [Exercise]?{
        return NSKeyedUnarchiver.unarchiveObject(withFile: Exercise.ArchiveURL.path) as? [Exercise]
    }
    
    func createAlertNetwork(title: String, message: String ){
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default , handler: { (action) in
            alert.dismiss(animated: true, completion: nil)
        }))
        
        self.present(alert, animated: true, completion: nil)
        
    }
    
    /*
     override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
     let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)
     
     // Configure the cell...
     
     return cell
     
     
     }
     */
    
    /*
     // Override to support conditional editing of the table view.
     override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the specified item to be editable.
     return true
     }
     */
    
    /*
     // Override to support editing the table view.
     override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
     if editingStyle == .delete {
     // Delete the row from the data source
     tableView.deleteRows(at: [indexPath], with: .fade)
     } else if editingStyle == .insert {
     // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
     }
     }
     */
    
    /*
     // Override to support rearranging the table view.
     override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    
    
}

