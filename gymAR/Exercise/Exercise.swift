//
//  Exercise.swift
//  gymAR
//
//  Created by Juan Diego González on 10/3/18.
//  Copyright © 2018 user145541. All rights reserved.
//

import UIKit
import os.log


class Exercise: NSObject, NSCoding {
    
    // MARK: Properties
    
    var name : String
    var photo : UIImage?
    var icon : UIImage?
    var urlVideo : String?
    var arImage : String?
    var descrip : String?
    var zone: String?
    
    //MARK: Archiving Paths
    
    static let DocumentsDirectory = FileManager().urls(for: .documentDirectory, in: .userDomainMask).first!
    static let ArchiveURL = DocumentsDirectory.appendingPathComponent("exercises")
    

    //MARK: Types
    struct PropertyKey{
        static let name = "name"
        static let urlVideo = "urlVideo"
        static let arImage = "arImage"
        static let descrip = "descrip"
        static let photo = "photo"
        static let icon = "icon"
        static let zone = "zone"
    }
    
    //MARK: Initialization
    init?( name: String, urlVideo : String, description : String, photo: UIImage, icon: UIImage, zone: String){
        
        // el nombre no debe estar vacío
        guard !name.isEmpty else{
            return nil
        }
        
        // la descripción no debe estar vacía
        guard !description.isEmpty else{
            return nil
        }
        
        // Inicialización
        self.name = name
        self.photo = photo
        self.urlVideo = urlVideo
        self.descrip = description
        self.icon = icon
        self.zone = zone
    }
    
    //MARK: NSCoding
    func encode(with aCoder: NSCoder) {
        aCoder.encode(name, forKey: PropertyKey.name)
        aCoder.encode(urlVideo, forKey: PropertyKey.urlVideo)
        aCoder.encode(arImage, forKey: PropertyKey.arImage)
        aCoder.encode(descrip, forKey: PropertyKey.descrip)
        aCoder.encode(photo, forKey: PropertyKey.photo)
        aCoder.encode(icon, forKey: PropertyKey.icon)
        aCoder.encode(zone, forKey: PropertyKey.zone)
    }
    
    required convenience init?(coder aDecoder: NSCoder) {
        
        guard let name = aDecoder.decodeObject(forKey: PropertyKey.name) as? String else {
            os_log("Unable to decode the name for a Exercise object.", log: OSLog.default, type: .debug)
            return nil
        }
        let urlVideo = aDecoder.decodeObject(forKey: PropertyKey.urlVideo) as! String
//        let arImage = aDecoder.decodeObject(forKey: PropertyKey.arImage) as! String
        let descrip = aDecoder.decodeObject(forKey: PropertyKey.descrip) as! String
        
        let photo = aDecoder.decodeObject(forKey: PropertyKey.photo) as! UIImage

        let icon = aDecoder.decodeObject(forKey: PropertyKey.icon) as! UIImage
        let zone = aDecoder.decodeObject(forKey: PropertyKey.zone) as! String
        self.init(name: name, urlVideo: urlVideo , description: descrip, photo: photo, icon: icon, zone: zone)
    }
    
}
