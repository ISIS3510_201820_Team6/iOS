//
//  StatisticValue.swift
//  gymAR
//
//  Created by Juan Diego González on 11/5/18.
//  Copyright © 2018 user145541. All rights reserved.
//

import UIKit
import os.log

class StatisticValue: NSObject, NSCoding {
    //MARK> Properties
    
    var peso : [Double]
    var grasa : [Double]
    var perimetro : [Double]
    var fecha : [String]
    
    //MARK: ArchivingPaths
    static let DocumentsDirectory = FileManager().urls(for: .documentDirectory, in: .userDomainMask).first!
    static let ArchiveURL = DocumentsDirectory.appendingPathComponent("statisticValue")
    
    //MARK: Types
    
    struct PropertyKey{
        static let peso = "peso"
        static let grasa = "grasa"
        static let perimetro = "perimetro"
        static let fecha = "fecha"
    }
    
    //MARK: Initialization
    
    init?(peso: [Double], grasa:  [Double], perimetro:  [Double], fecha: [String]){
        self.peso = peso
        self.grasa = grasa
        self.perimetro = perimetro
        self.fecha = fecha
    }
    
    //MARK: NSCOding
    func encode(with aCoder: NSCoder){
        aCoder.encode(peso, forKey: PropertyKey.peso)
        aCoder.encode(grasa, forKey: PropertyKey.grasa)
        aCoder.encode(perimetro, forKey: PropertyKey.perimetro)
        aCoder.encode(fecha, forKey: PropertyKey.fecha)
        
    }
    
    required convenience init?(coder aDecoder: NSCoder) {
        let peso = aDecoder.decodeObject(forKey: PropertyKey.peso) as! [Double]
        let grasa = aDecoder.decodeObject(forKey: PropertyKey.grasa) as! [Double]
        let perimetro = aDecoder.decodeObject(forKey: PropertyKey.perimetro) as! [Double]
        let fecha = aDecoder.decodeObject(forKey: PropertyKey.fecha) as! [String]
        self.init(peso: peso, grasa: grasa, perimetro: perimetro, fecha: fecha)
    }
}
