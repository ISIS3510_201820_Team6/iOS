//
//  PlanViewController.swift
//  gymAR
//
//  Created by user145541 on 10/3/18.
//  Copyright © 2018 user145541. All rights reserved.
//

import UIKit
import Firebase
import os.log

class PlanViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    @IBOutlet weak var planTypeLabel: UILabel!
    @IBOutlet weak var tableview: UITableView!
    @IBOutlet weak var weeksLabel: UILabel!
    @IBOutlet weak var daysPerWeekLabel: UILabel!
    @IBOutlet weak var segmentedButton: UISegmentedControl!
    var plan: Plan!
    var selectedDay = 0
    var uid: String!
    var queryRef: Query!
    var downloader: PlanDownloader!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        downloader = PlanDownloader()
        uid = UserDefaults.standard.value(forKey: "userID") as? String ?? ""
        queryRef = Firestore.firestore().collection("usuario_plan").whereField("uId", isEqualTo: uid).order(by: "vencimiento", descending: true).limit(to: 1)
        
            let planCargado = loadPlan()
            if planCargado == nil {
                if CheckNetwork.Connection(){
                    DispatchQueue.global(qos: .utility).async {
                        self.downloader.downloadPlan(queryRef: self.queryRef){ elPlan in
                            self.plan = elPlan
                            DispatchQueue.main.async{
                                self.updatePView()
                            }
                        }
                    }
                } else{
//                    waitForConnection()
                    createAlertNetwork(title: "No tienes conexión a Internet", message: "Para ayudarte con el entrenamiento, cargaremos algunos ejercicios.")
                }
            } else{
                plan = planCargado
                DispatchQueue.global(qos: .background).async {
                    self.updatePlan()
                }
                let diasPs = self.plan.diasPorSemana ?? 0
                updateSegmentButton(diasPs: diasPs)
                tableview.reloadData()
            }
        
    }
    
    func updatePlan(){
        guard CheckNetwork.Connection() == true else {return}
        queryRef.addSnapshotListener { (querySnapshot, error) in
            guard let snapshot = querySnapshot else {
                print("Error fetching snapshots: \(error!)")
                return
            }
            let data = querySnapshot?.documents[0].data()
            let plan = data?["plan"] as! DocumentReference
            let id = plan.documentID
            if id != self.plan.id{
                self.downloader.downloadPlan(queryRef: self.queryRef){ elPlan in
                        print("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa")
                        self.plan = elPlan
                        DispatchQueue.main.async {
                            self.updatePView()
                        }
                }
            }
        }
    }
    
    func updatePView(){
        selectedDay = 0
        let diasPs = self.plan.diasPorSemana ?? 0
        self.planTypeLabel.text = "\(plan.type ?? "") Plan"
        self.weeksLabel.text = "\(plan.semanas ?? 0) Weeks"
        self.daysPerWeekLabel.text = "\(plan.diasPorSemana ?? 0) Days per week"
        self.updateSegmentButton(diasPs: diasPs)
        self.tableview.reloadData()
        print("guardando")
        self.savePlan()
    }
    
    func waitForConnection(){
        while true{
            DispatchQueue.global(qos: .background).asyncAfter(deadline: .now() + .milliseconds(500)){
                if CheckNetwork.Connection(){
                    DispatchQueue.main.async {
                        self.updatePView()
                    }
                }
            }
        }
    }
    
    func updateSegmentButton(diasPs: Int){
        self.segmentedButton.removeAllSegments()
        for index in 0..<diasPs {
            self.segmentedButton.insertSegment(withTitle: plan.routines[index].name, at: index, animated: false)
        }
        self.segmentedButton.selectedSegmentIndex = 0
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if plan == nil{
            return 0
        }else{
            print(plan.routines.count)
            return plan.routines[selectedDay].exercises.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "routineCell", for: indexPath) as? routineTableViewCell else {
            fatalError("The dequeued cell is not an instance of routineTableViewCellelse.")
        }
        let exerciseCell = plan.routines[selectedDay].exercises[indexPath.row]
        cell.excersiceName.text = exerciseCell.name
        cell.excerciseSets.text = "\(exerciseCell.series ?? 0) sets \(exerciseCell.reps ?? 0) reps"
        cell.exerciseImg.image = exerciseCell.icon
        return cell
    }
    
    @IBAction func segmentedAction(_ sender: UISegmentedControl) {
        selectedDay = sender.selectedSegmentIndex
        tableview.reloadData()
    }
    
    //MARK: Private Methods
    private func savePlan(){
        let isSuccessfulSave = NSKeyedArchiver.archiveRootObject(plan, toFile: Plan.ArchiveURL.path)
        if isSuccessfulSave{
            UserDefaults.standard.set(true, forKey: "savedPlan")
            os_log("Exerci ses successfully saved", log: OSLog.default, type: .debug)
        } else {
            os_log("Failed to save exercises", log: OSLog.default, type: .error)
        }
    }
    
    private func loadPlan()-> Plan?{
        return NSKeyedUnarchiver.unarchiveObject(withFile: Plan.ArchiveURL.path) as? Plan
    }
    
    func createAlertNetwork(title: String, message: String ){
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default , handler: { (action) in
            alert.dismiss(animated: true, completion: nil)
        }))
        
        self.present(alert, animated: true, completion: nil)
        
    }
    // MARK: - Navigationsdfsdfsssdfsdffds
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segueDetailFromPlan"{
            if let indexPath = tableview.indexPathForSelectedRow{
                let destVC = segue.destination as! ExerciseViewController
                destVC.name = plan.routines[selectedDay].exercises[indexPath.row].name
                destVC.desc = plan.routines[selectedDay].exercises[indexPath.row].descrip
                destVC.image = plan.routines[selectedDay].exercises[indexPath.row].photo
                destVC.videoUrl = plan.routines[selectedDay].exercises[indexPath.row].urlVideo
            }
        }
    }
    /*
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
