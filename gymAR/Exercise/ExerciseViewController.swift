//
//  ExerciseViewController.swift
//  gymAR
//
//  Created by Juan Diego González on 10/3/18.
//  Copyright © 2018 user145541. All rights reserved.
//

import UIKit
import Firebase


class ExerciseViewController : UIViewController {
    //MARK: Properties
    var name: String?
    var image: UIImage?
    var desc: String?
    var videoUrl: String?
    var zona: String?
    var collectionRef: CollectionReference!
    var nombreEnvio : String?
    var imagenEnvio : String?

    
    @IBOutlet weak var callButton: UIButton!
    @IBOutlet weak var arButton: UIButton!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var peroMiraEsaDesc: UITextView!
    @IBOutlet weak var laGranImagen: UIImageView!
    @IBOutlet weak var elTextazo: UITextView!
    @IBOutlet weak var noInternetImage: UIImageView!
    @IBOutlet weak var elVideo: UIWebView!
    //MARK: View Lifecycle

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
            if let name = self.name{
               self.nameLabel.text = name
            }
            
            if let image = self.image{
                do{
                    self.laGranImagen.image = image
                } catch{
                    print(error)
                }
            }
            if let desc = self.desc {
                self.peroMiraEsaDesc.text = ""
                print("Hola que hace" )
                let elArreglo = desc.split(separator: "@")
                print(elArreglo.count)
                for frase in elArreglo{
                    print(frase)
                    self.peroMiraEsaDesc.text.append(String(frase))
                    self.peroMiraEsaDesc.text.append("\n")
                }
            }
            if let videoUrl = self.videoUrl {
                self.getVideo(videoCode: videoUrl)
            }

        
    }
    
    
    func getVideo(videoCode: String){
        if CheckNetwork.Connection(){
            noInternetImage.alpha = 0
            let url = URL(string: "https://www.youtube.com/embed/\(videoCode)")
            elVideo.loadRequest(URLRequest(url: url!))
        } else {
            self.createAlert(title: "No Hay conexión a Internet", message: "Lo sentimos, no podremos mostrar el video ni podrás llamar a un entrenador.")
            noInternetImage.alpha = 1
            
        }
        
        
    }
    
    @IBAction func arClick(_ sender: Any) {
        let url = URL(string: "ARCustomUrlScheme://")!
        if UIApplication.shared.canOpenURL(url){
            UIApplication.shared.open(url)
        }else{
            createAlert(title: "Error al intentar abrir AR", message: "No fue posible abrir la aplicacion de Realidad Aumentada, verifique si esta instalada")
        }
    }
    
    
    @IBAction func callClick(_ sender: Any) {
        
         if CheckNetwork.Connection(){
            
            let idRequerido = UserDefaults.standard.value(forKey: "userID") as? String ?? ""
            let venAquiRapido = Firestore.firestore().document("usuarios/\(idRequerido)")
            venAquiRapido.getDocument{ (document, error) in
                if let document = document , document.exists {
                    let data = document.data()
                    let nombre = data?["nombre"] as? String ?? ""
                    let img = data?["imagen"] as? String ?? ""
                    self.collectionRef = Firestore.firestore().collection("solicitudesEntrenadores")
                    let timeStamp = NSDate().timeIntervalSince1970
                    let bonyurtHijadePuta = TimeInterval(timeStamp)
                    let laAmigaBuenona = NSDate(timeIntervalSince1970: bonyurtHijadePuta)
                    let jijueputaZona = self.zona ?? ""
                    print(jijueputaZona)
                    let docData: [String: Any] = [
                        "cliente" : nombre,
                        "IDCliente": idRequerido,
                        "imagen": img,
                        "ejercicio" : self.name!,
                        "fechaSolicitud": laAmigaBuenona,
                        "realizada": false,
                        "zona" :jijueputaZona
                    ]
                    self.collectionRef.addDocument(data: docData){
                        err in
                        if let err = err {
                            print("Error adding document: \(err)")
                        }else{
                            print("Document added")
                            self.createAlert(title: "En Camino", message: "Un entrador está corriendo hacia donde tú estás.")
                        }
                    }
                    
                }else{
                    print("Document does not exist")
                }
                
            }
         } else {
            self.createAlert(title: "No tienes conexión a Internet", message: "En este momento no podrás llamar a un entrenador. Intentalo más tarde.")
        }
        
       
    }
    func getMen(){
        
    }
    
    func createAlert(title: String, message: String ){
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default , handler: { (action) in
            alert.dismiss(animated: true, completion: nil)
        }))
        
        self.present(alert, animated: true, completion: nil)
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        arButton.backgroundColor = .clear
        arButton.layer.cornerRadius = 5
        arButton.layer.borderWidth = 1
        arButton.layer.borderColor = UIColor.black.cgColor
        
        callButton.backgroundColor = .clear
        callButton.layer.cornerRadius = 5
        callButton.layer.borderWidth = 1
        callButton.layer.borderColor = UIColor.black.cgColor
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
}
