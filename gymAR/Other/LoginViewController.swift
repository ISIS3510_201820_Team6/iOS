//
//  LoginViewController.swift
//  gymAR
//
//  Created by NICOLAS EDUARDO CABRERA VENEGAS on 10/25/18.
//  Copyright © 2018 user145541. All rights reserved.
//

import UIKit
import FirebaseAuth
import Firebase

class LoginViewController: UIViewController, UITextFieldDelegate{

    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var passworTextField: UITextField!
    @IBOutlet weak var errorLabel: UILabel!
    var docRef: DocumentReference!
    override func viewDidLoad() {
        super.viewDidLoad()
        errorLabel.isHidden = true
        usernameTextField.keyboardType = UIKeyboardType.emailAddress
        usernameTextField.delegate = self
        passworTextField.delegate = self
        // Do any additional setup after loading the view.
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        // Hide the keyboard.
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    @IBAction func loginClickButton(_ sender: UIButton) {
        
        if usernameTextField.text != "" && passworTextField.text != "" {
            Auth.auth().signIn(withEmail: usernameTextField.text!, password: passworTextField.text!){
                (user, error) in
                if user != nil{
                    UserDefaults.standard.set(user!.uid, forKey: "userID")
                    self.docRef = Firestore.firestore().document("usuarios/\(user!.uid)")
                    self.docRef.addSnapshotListener{(docSnapshot, error) in
                        guard let docSnapshot = docSnapshot, docSnapshot.exists else {
                            return
                        }
                        let data = docSnapshot.data()
                        let rol = data?["rol"] as? String ?? ""
//                        print("bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb")
                        UserDefaults.standard.set(rol, forKey: "rol")
                        if rol == "entrenador"{
                            self.performSegue(withIdentifier: "loginTrainer", sender: self)
                        }else{
                            self.performSegue(withIdentifier: "loginSegue", sender: self)
                        }
                    }
                } else {
                    if !CheckNetwork.Connection(){
                        self.errorLabel.text = "No hay conexión a internet"
                    }
                    self.errorLabel.isHidden = false
                }
            }
        } else {
            self.errorLabel.isHidden = false
        }
    }
}
