//
//  MuscleValue.swift
//  gymAR
//
//  Created by Juan Diego González on 11/5/18.
//  Copyright © 2018 user145541. All rights reserved.
//

import UIKit
import os.log


class MuscleValue: NSObject, NSCoding {
    
    // MARK: Properties
    var muscles : [String]
    var values : [[Double]]
    
    //MARK: Arhciving Paths
    static let DocumentsDirectory = FileManager().urls(for: .documentDirectory, in: .userDomainMask).first!
    static let ArchiveURL = DocumentsDirectory.appendingPathComponent("muscleValue")
    
    //MARK: Types
    
    struct PropertyKey{
        static let muscles = "muscles"
        static let values = "values"
    }
    
    //MARK: Initialization
    
    init?(muscles: [String], values: [[Double]]){
        self.muscles = muscles
        self.values = values
    }
    
    //MARK: NSCoding
    
    func encode(with aCoder: NSCoder){
        aCoder.encode(muscles, forKey: PropertyKey.muscles)
        aCoder.encode(values, forKey: PropertyKey.values)
        
    }
    
    required convenience init?(coder aDecoder: NSCoder) {
        let muscles = aDecoder.decodeObject(forKey: PropertyKey.muscles) as! [String]
        let values = aDecoder.decodeObject(forKey: PropertyKey.values) as! [[Double]]
        self.init(muscles: muscles, values: values)
    }
    
    
}
