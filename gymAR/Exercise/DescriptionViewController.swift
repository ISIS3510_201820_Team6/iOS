//
//  DescriptionViewController.swift
//  gymAR
//
//  Created by Juan Diego González on 10/6/18.
//  Copyright © 2018 user145541. All rights reserved.
//

import UIKit

class DescriptionViewController: UIViewController {
    var name: String?
    var desc: String?
    var image: UIImage?
    
    @IBOutlet weak var imagePhoto: UIImageView!
    @IBOutlet weak var descriptionText: UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let desc = desc{
            descriptionText.text = desc
        }
        if let image = image{
            do {
                self.imagePhoto.image = image
            }
            catch{
                print(error)
            }
        }
        
    }
    // IMPORTANTE PARA SACAR LAS FOTOS DE INTERNET
    //    do {
    //    let url = URL(string: "http://verona-api.municipiumstaging.it/system/images/image/image/22/app_1920_1280_4.jpg")
    //    let data = try Data(contentsOf: url)
    //    self.imageView.image = UIImage(data: data)
    //    }
    //    catch{
    //    print(error)
    //    }
    
}
