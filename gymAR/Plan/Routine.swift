//
//  Routine.swift
//  gymAR
//
//  Created by user145541 on 10/4/18.
//  Copyright © 2018 user145541. All rights reserved.
//

import UIKit

class Routine: NSObject, NSCoding {
    
    var exercises: [RoutineExercise]!
    var name: String!
    //MARK: Archiving Paths
    
    static let DocumentsDirectory = FileManager().urls(for: .documentDirectory, in: .userDomainMask).first!
    static let ArchiveURL = DocumentsDirectory.appendingPathComponent("routines2")
    
    //MARK: Types
    struct PropertyKey{
        static let exercises = "exercises"
        static let name = "name"
    }
    
    init(exercises: [RoutineExercise], name: String) {
        self.exercises = exercises
        self.name = name
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(exercises, forKey: PropertyKey.exercises)
        aCoder.encode(name, forKey: PropertyKey.name)
    }
    
    required convenience init?(coder aDecoder: NSCoder) {
        let exercises = aDecoder.decodeObject(forKey: PropertyKey.exercises) as! [RoutineExercise]
        let name = aDecoder.decodeObject(forKey: PropertyKey.name) as! String
        self.init(exercises: exercises, name: name)
    }
}
