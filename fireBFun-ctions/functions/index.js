const functions = require("firebase-functions");
const admin = require("firebase-admin");
admin.initializeApp();
// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
// exports.helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });
exports.assignClientToTrainer = functions.firestore
	.document("solicitudesEntrenadores/{idSolicitud}")
	.onCreate((snap) => {
		const data = snap.data();
		const zone = data.zona;
		const cliRef = data.cliente;
		const cliImg = data.imagen;
		const fecha = data.fechaSolicitud;
		const ejercicio = data.ejercicio;
		let query = admin.firestore().collection("usuarios").where("rol", "==", "entrenador").where("zona", "==", zone).orderBy("asignados", "asc");
		return query.get().then(querySnapshot => {
			var qSnap = querySnapshot.docs[0];
			var trainer = qSnap.id;
			var trainerData = qSnap.data();
			var assigned = trainerData.asignados;
			var trainerRef = admin.firestore().doc(`usuarios/${trainer}`);
			var collection = admin.firestore().collection("trainerUser");
			let obj = {
				cliente: cliRef,
				imagen: cliImg,
				fechaSolicitud: fecha,
				tId: trainer,
				realizada: false,
				ejercicio: ejercicio
			};
			collection.add(obj);
			trainerRef.update({asignados: assigned + 1});
		});
	});